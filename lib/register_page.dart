import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gin_finans_haikal/router_constants.dart';
import 'package:gin_finans_haikal/step2/step2.dart';
import 'package:gin_finans_haikal/step3/step3.dart';
import 'package:gin_finans_haikal/step4/step4.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'step0/step0.dart';
import 'step1/step1.dart';
import 'widget/next_button.dart';
import 'package:gin_finans_haikal/theme/theme_color.dart';

import 'widget/step_bar_button.dart';

const _horizontalPadding = 24.0;

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final GlobalKey<FormState> _formRegister = GlobalKey<FormState>();
  TextEditingController _email = TextEditingController();
  DateTime selectedDate = DateTime.now();
  TimeOfDay selectedTime = TimeOfDay(hour: 00, minute: 00);
  int currentPage = 0;
  bool currentPageEnd = false;
  bool passError = false;
  bool emailError = false;
  var goalError = 0;
  var incomeError = 0;
  var expenseError = 0;
  var emailValue = "";
  var passwordValue = "";
  var goalValue = "";
  var incomeValue = "";
  var expenseValue = "";
  var tanggalValue = "";
  var waktuValue = "";
  var appBarTitle = "Create Account";

  var inputFormat = DateFormat('dd/MM/yyyy');

  final passwordValidation =
      RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{9,}$');

  final emailValidation = RegExp(
      r'^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initializeDateFormatting();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        setState(() {
          currentPage--;
          if (currentPage < 0) {
            currentPage = 0;
            currentPageEnd = true;
          }
          if (currentPage == 0) {
            passwordValue = "";
            passError = false;
          }
          if (currentPage == 1) {
            goalValue = "";
            goalError = 0;
            incomeValue = "";
            incomeError = 0;
            expenseValue = "";
            expenseError = 0;
          }
          if (currentPage == 2) {
            tanggalValue = "";
            waktuValue = "";
          }
        });
        if (currentPageEnd == true) {
          setState(() {
            currentPageEnd = false;
          });
          return _onWillPop();
        }
        return Future.value();
      },
      child: Scaffold(
        appBar: currentPage != 0
            ? AppBar(
                elevation: 0.0,
                iconTheme: IconThemeData(
                    color: currentPage != 0
                        ? AppColor.deepCerulean
                        : AppColor.wildSand),
                title: Text(appBarTitle,
                    style: TextStyle(
                      fontSize: 16.0,
                      color: AppColor.wildSand,
                      fontFamily: "Sora",
                    )),
                leading: IconButton(
                  icon: Icon(
                    Icons.arrow_back,
                    color: AppColor.wildSand,
                  ),
                  onPressed: () {
                    setState(() {
                      currentPage--;
                      if (currentPage < 0) {
                        currentPage = 0;
                        currentPageEnd = true;
                      }
                      if (currentPage == 0) {
                        passwordValue = "";
                        passError = false;
                      }
                      if (currentPage == 1) {
                        goalValue = "";
                        goalError = 0;
                        incomeValue = "";
                        incomeError = 0;
                        expenseValue = "";
                        expenseError = 0;
                      }
                      if (currentPage == 2) {
                        tanggalValue = "";
                        waktuValue = "";
                      }
                    });
                    if (currentPageEnd == true) {
                      setState(() {
                        currentPageEnd = false;
                      });
                      _onWillPop();
                    }
                  },
                ),
                backgroundColor: AppColor.deepCerulean,
              )
            : AppBar(
                elevation: 0.0,
                iconTheme: IconThemeData(color: AppColor.wildSand),
                title: Text(""),
                backgroundColor: AppColor.wildSand,
              ),
        // resizeToAvoidBottomInset: false,

        backgroundColor:
            currentPage <= 0 ? AppColor.wildSand : AppColor.deepCerulean,

        body: SafeArea(
            child: Stack(
          children: [
            // Container(decoration: new BoxDecoration(
            //     image: DecorationImage(
            //         image:  AssetImage("assets/images/1.JPG"),
            //         fit: BoxFit.cover
            //     ))),
            Column(
              children: [
                Flexible(
                  flex: 1,
                  child: Form(
                    key: _formRegister,
                    child: ListView(
                      physics: const ClampingScrollPhysics(),
                      padding: const EdgeInsets.symmetric(
                        horizontal: _horizontalPadding,
                      ),
                      children: [
                        SizedBox(height: 20),

                        // _AppLogo(),
                        Widget_StepBar(currentPage),

                        currentPage == 0
                            ? Step0(_email, emailError, (email) {
                                setState(() {
                                  emailValue = email;
                                  if (currentPage == 0) {
                                    if (emailValidation.hasMatch(emailValue) ||
                                        emailValue == "") {
                                      setState(() {
                                        emailError = false;
                                      });
                                    } else {
                                      emailError = true;
                                    }
                                  }
                                });
                              })
                            : Container(),

                        currentPage == 1
                            ? Step1(currentPage, passwordValue, passError,
                                (password) {
                                setState(() {
                                  passwordValue = password;
                                  // passError = false;

                                  if (currentPage == 1) {
                                    if (passwordValidation
                                            .hasMatch(passwordValue) ||
                                        passwordValue == "") {
                                      setState(() {
                                        passError = false;
                                      });
                                    } else {
                                      passError = true;
                                    }
                                  }
                                });
                              })
                            : Container(),

                        currentPage == 2
                            ? Step2(
                                goalValue: goalValue,
                                goalError: goalError,
                                goalFunction: (goal) {
                                  setState(() {
                                    goalValue = goal.toString();
                                    if (currentPage == 2) {
                                      if (goalValue != "" &&
                                          goalValue != "-Choose Option-") {
                                        setState(() {
                                          goalError = 0;
                                        });
                                      } else {
                                        goalError = 1;
                                      }
                                    }
                                  });
                                },
                                incomeValue: incomeValue,
                                incomeError: incomeError,
                                incomeFunction: (income) {
                                  setState(() {
                                    incomeValue = income.toString();
                                    if (currentPage == 2) {
                                      if (incomeValue != "" &&
                                          incomeValue != "-Choose Option-") {
                                        setState(() {
                                          incomeError = 0;
                                        });
                                      } else {
                                        incomeError = 1;
                                      }
                                    }
                                  });
                                },
                                expenseValue: expenseValue,
                                expenseError: expenseError,
                                expenseFunction: (expense) {
                                  setState(() {
                                    expenseValue = expense.toString();
                                    if (currentPage == 2) {
                                      if (expenseValue != "" &&
                                          expenseValue != "-Choose Option-") {
                                        setState(() {
                                          expenseError = 0;
                                        });
                                      } else {
                                        expenseError = 1;
                                      }
                                    }
                                  });
                                },
                              )
                            : Container(),
                        currentPage == 3
                            ? Step3(
                                tanggalValue: tanggalValue,
                                tanggalFunction: () {
                                  _selectDate(context);
                                },
                                waktuValue: waktuValue,
                                waktuFunction: () {
                                  _selectTime(context);
                                },
                              )
                            : Container(),
                        currentPage == 4
                            ? Step4()
                            : Container(),

                        // SizedBox(height: 16),
                      ],
                    ),
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      child: Widget_NextButton(
                        page: currentPage,
                        function: () {
                          if (currentPage == 0) {
                            if (emailValidation.hasMatch(emailValue)) {
                              setState(() {
                                currentPage++;
                              });
                            }
                          } else if (currentPage == 1) {
                            if (passwordValidation.hasMatch(passwordValue)) {
                              setState(() {
                                currentPage++;
                                passError = false;
                                passwordValue = "";
                              });
                            }
                            // else {
                            //   passError = true;
                            // }
                          } else if (currentPage == 2) {
                            if ((goalValue != "" &&
                                    goalValue != "-Choose Option-") &&
                                (incomeValue != "" &&
                                    incomeValue != "-Choose Option-") &&
                                (expenseValue != "" &&
                                    expenseValue != "-Choose Option-")) {
                              setState(() {
                                currentPage++;
                                goalError = 0;
                                incomeError = 0;
                                expenseError = 0;
                              });
                            }
                          } else if (currentPage == 3) {
                            if (tanggalValue != "" && waktuValue != "") {
                              setState(() {
                                currentPage++;
                              });
                            }
                          } else if (currentPage == 4) {
                            _onWillPop();
                            }
                          }
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        )),
      ),
    );
  }

  Future<Null> _selectTime(BuildContext context) async {
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: selectedTime,
    );
    if (picked != null)
      setState(() {
        selectedTime = picked;
        var _hour = selectedTime.hour.toString().length < 2
            ? "0" + selectedTime.hour.toString()
            : selectedTime.hour.toString();
        var _minute = selectedTime.minute.toString().length < 2
            ? "0" + selectedTime.minute.toString()
            : selectedTime.minute.toString();
        waktuValue = _hour + ':' + _minute;
      });
  }

  Future<Null> _selectDate(BuildContext context) async {
    // Initial DateTime FIinal Picked
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        String hari = DateFormat.EEEE('id').format(picked).toString();
        String tgl = DateFormat.d('id').format(picked).toString();
        String bulan = DateFormat.MMM('id').format(picked).toString();
        String tahun = DateFormat.y('id').format(picked).toString();
        tanggalValue = "$hari, $tgl $bulan $tahun";
      });
  }

  Future<bool> _onWillPop() async {
    return await showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: const Text('do you want to exit this application ?'),
            content: const Text(''),
            actions: <Widget>[
              FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: const Text('No'),
              ),
              FlatButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: const Text('Yes'),
              ),
            ],
          ),
        ) ??
        false;
  }
}
