import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gin_finans_haikal/theme/theme_color.dart';
import 'package:flutter/services.dart';

// ignore: camel_case_types
class Widget_NextButton extends StatelessWidget {
  int page = 0 ;
  Function()? function;
  Widget_NextButton({this.function, this.page=0});

  static const largeLetterSpacing = 1.0;
  double letterSpacingOrNone(double letterSpacing) =>
      kIsWeb ? 0.0 : letterSpacing;

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      color: Colors.blue,
      child: Padding(
        padding: EdgeInsets.zero,
        child: Text(
          page <4?
          "Next" : "Finish",
          style: TextStyle(
              color: Colors.white,
              letterSpacing: letterSpacingOrNone(largeLetterSpacing)),
        ),
      ),
      elevation: 8,
      // shape:  RoundedRectangleBorder(
      //   borderRadius: BorderRadius.all(Radius.circular(7)),
      // ),
      onPressed: function

    );
  }
}