import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gin_finans_haikal/theme/theme_color.dart';
import 'package:flutter/services.dart';
import 'ceklis.dart';

// ignore: camel_case_types
class Widget_ValidasiBar extends StatelessWidget {
  int page = 0;
  String password = "";

  Widget_ValidasiBar(this.password, this.page);

  final lowerCase = RegExp(r'^(?=.*[a-z])');

  bool lowerCaseValue = false;

  final upperCase = RegExp(r'^(?=.*[A-Z])');
  bool upperCaseValue = false;

  final numberCase = RegExp(r'^(?=.*[0-9])');
  bool numberCaseValue = false;

  bool lengthCaseValue = false;

  final upperLowerCase = RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])$');

  @override
  Widget build(BuildContext context) {
    lowerCaseValue = page == 1 && lowerCase.hasMatch(password) ? true : false;
    upperCaseValue = page == 1 && upperCase.hasMatch(password) ? true : false;
    numberCaseValue = page == 1 && numberCase.hasMatch(password) ? true : false;
    lengthCaseValue = page == 1 && password.length >= 9 ? true : false;
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      // mainAxisAlignment: MainAxisAlignment.center,
      children: [
        lowerCaseValue
            ? Ceklis()
            : Container(
                child: Column(
                  children: [
                    Container(
                        width: 60,
                        height: 60,
                        child: Center(
                          child: Text(
                            "a",
                            style: TextStyle(
                                color: AppColor.wildSand, fontSize: 40),
                          ),
                        )),
                    Container(
                        width: 60,
                        height: 10,
                        child: Center(
                          child: Text(
                            "Lowercase",
                            style: TextStyle(
                                color: AppColor.wildSand, fontSize: 12),
                          ),
                        )),
                  ],
                ),
              ),

        upperCaseValue
            ? Ceklis()
            : Container(
                child: Column(
                  children: [
                    Container(
                        width: 60,
                        height: 60,
                        child: Center(
                          child: Text(
                            "A",
                            style: TextStyle(
                                color: AppColor.wildSand, fontSize: 40),
                          ),
                        )),
                    Container(
                        width: 60,
                        height: 10,
                        child: Center(
                          child: Text(
                            "Uppercase",
                            style: TextStyle(
                                color: AppColor.wildSand, fontSize: 12),
                          ),
                        )),
                  ],
                ),
              ),

        numberCaseValue
            ? Ceklis()
            : Container(
                child: Column(
                  children: [
                    Container(
                        width: 60,
                        height: 60,
                        child: Center(
                          child: Text(
                            "123",
                            style: TextStyle(
                                color: AppColor.wildSand, fontSize: 30),
                          ),
                        )),
                    Container(
                        width: 60,
                        height: 10,
                        child: Center(
                          child: Text(
                            "Number",
                            style: TextStyle(
                                color: AppColor.wildSand, fontSize: 12),
                          ),
                        )),
                  ],
                ),
              ),

        lengthCaseValue
            ? Ceklis()
            : Container(
                child: Column(
                  children: [
                    Container(
                        width: 60,
                        height: 60,
                        child: Center(
                          child: Text(
                            "9+",
                            style: TextStyle(
                                color: AppColor.wildSand, fontSize: 30),
                          ),
                        )),
                    Container(
                        width: 60,
                        height: 10,
                        child: Center(
                          child: Text(
                            "Characters",
                            style: TextStyle(
                                color: AppColor.wildSand, fontSize: 12),
                          ),
                        )),
                  ],
                ),
              ),
        // ceklis()
      ],
    );
  }
}
