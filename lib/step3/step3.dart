import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gin_finans_haikal/step3/widget/tanggal_dropdown.dart';
import 'package:gin_finans_haikal/step3/widget/waktu_dropdown.dart';
import 'package:gin_finans_haikal/theme/theme_color.dart';

class Step3 extends StatefulWidget {
  // int page = 0;
  // String passwordValue;
  // bool passwordError;
  var tanggalError = 0;
  Function() tanggalFunction;
  String tanggalValue;
  var waktuError = 0;
  Function() waktuFunction;
  String waktuValue;

  Step3({
    this.tanggalValue = "",
    this.tanggalError = 0,
    required this.tanggalFunction,
    this.waktuValue = "",
    this.waktuError = 0,
    required this.waktuFunction,
  });

  @override
  _Step3State createState() => _Step3State();
}

class _Step3State extends State<Step3> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 100),
        TanggalDropdown(widget.tanggalFunction, widget.tanggalValue, widget.tanggalError),
        WaktuDropdown(widget.waktuFunction, widget.waktuValue, widget.waktuError)
      ],
    );
  }
}
