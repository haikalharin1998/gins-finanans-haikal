import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gin_finans_haikal/theme/theme_color.dart';

class GoalDropdown extends StatefulWidget {
  Function(Object?) function;
  String goalValue;
  int goalError = 0;

  GoalDropdown(this.function, this.goalValue, this.goalError);

  @override
  _GoalDropdownState createState() => _GoalDropdownState();
}

class _GoalDropdownState extends State<GoalDropdown> {
  final List<String> itemsList = <String>[
    "-Choose Option-",
    "acceleration",
    "Coding champion",
    "Cumlaude",
    "Best of the best",
  ];

  String _hint = 'Pilih Kecamatan';

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return FormField(
      // key: _fieldKey,
      enabled: true,
      builder: (FormFieldState<dynamic> field) {
        return InputDecorator(
          decoration: InputDecoration(
            contentPadding: EdgeInsets.only(top: 10.0, bottom: 0.0),
            border: InputBorder.none,
            errorText: widget.goalError == 1 ? 'Please Choose' : null,
          ),
          child: Container(
            decoration: BoxDecoration(
              color: AppColor.wildSand,
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            padding: EdgeInsets.only(
              top: 15,
            ),
            child: DropdownButtonFormField(
              decoration: InputDecoration(
                labelText: 'Goal for activation',
                contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                fillColor: AppColor.wildSand,
                filled: true,
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide.none),
              ),
              // hint: Text(_hint),
              isExpanded: true,
              items: itemsList.map((option) {
                return DropdownMenuItem(
                  child: Text("$option"),
                  value: option,
                );
              }).toList(),
              value: widget.goalValue == ""
                  ? "-Choose Option-"
                  : itemsList.contains(widget.goalValue)
                      ? widget.goalValue
                      : null,
              onChanged: widget.function,
            ),
          ),
        );
      },
    );
  }
}
