import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gin_finans_haikal/theme/theme_color.dart';
import 'package:flutter/services.dart';

// ignore: camel_case_types
class Widget_StepBar extends StatelessWidget {
 var page = 0;
  Widget_StepBar(this.page);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
            width: 60,
            height: 60,
            decoration: new BoxDecoration(
              border: Border.all(color: Colors.black, width: 2),
              color: page < 1 ? AppColor.white: AppColor.green,
              shape: BoxShape.circle,
            ),
            child: Center(
              child: Text("1",style: TextStyle(color: AppColor.black, fontSize: 20),),
            )        ),
        Container(
            width: 30,
            height: 0,
            decoration: new BoxDecoration(
              border: Border.all(color: Colors.black, width: 2),
              color: AppColor.white,
            ),
        ),
        Container(
            width: 60,
            height: 60,
            decoration: new BoxDecoration(
              border: Border.all(color: Colors.black, width: 2),
              color: page < 2 ? AppColor.white: AppColor.green,
              shape: BoxShape.circle,
            ),
            child: Center(
              child: Text("2",style: TextStyle(color: AppColor.black, fontSize: 20),),
            )        ),
        Container(
            width: 30,
            height: 0,
            decoration: new BoxDecoration(
              border: Border.all(color: Colors.black, width: 2),
              color: AppColor.white,
            ),
        ),
        Container(
            width: 60,
            height: 60,
            decoration: new BoxDecoration(
              border: Border.all(color: Colors.black, width: 2),
              color: page < 3 ? AppColor.white: AppColor.green,
              shape: BoxShape.circle,
            ),
            child: Center(
              child: Text("3",style: TextStyle(color: AppColor.black, fontSize: 20),),
            )        ),
        Container(
            width: 30,
            height: 0,
            decoration: new BoxDecoration(
              border: Border.all(color: Colors.black, width: 2),
              color: AppColor.white,
            ),),
        Container(
            width: 60,
            height: 60,
            decoration: new BoxDecoration(
              border: Border.all(color: Colors.black, width: 2),
              color: page < 4 ? AppColor.white: AppColor.green,
              shape: BoxShape.circle,
            ),
            child: Center(
              child: Text("4",style: TextStyle(color: AppColor.black, fontSize: 20),),
            )
        )
      ],
    );
  }
}
