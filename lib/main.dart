import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gin_finans_haikal/register_page.dart';
import 'package:gin_finans_haikal/routes.dart';
import 'widget/next_button.dart';
import 'package:gin_finans_haikal/theme/theme_color.dart';

import 'widget/step_bar_button.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    onGenerateRoute: Routes.generateRoute,
    home: RegisterPage(),
  ));
}
