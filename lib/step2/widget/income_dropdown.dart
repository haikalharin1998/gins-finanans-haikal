import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gin_finans_haikal/theme/theme_color.dart';


class IncomeDropdown extends StatefulWidget {
  Function(Object?) function;
  String incomeValue;
  int incomeError = 0;
  IncomeDropdown(this.function,this.incomeValue, this.incomeError);

  @override
  _IncomeDropdownState createState() => _IncomeDropdownState();
}

class _IncomeDropdownState extends State<IncomeDropdown> {
  final List<String> itemsList = <String>["-Choose Option-", "< 5.000.000",
    "5.000.000 - 10.000.000",
    "10.000.000 - 2.000.000",
    "20.000.000 <",
  ];

  String _hint = 'Pilih Kecamatan';

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return FormField(
      // key: _fieldKey,
      enabled: true,
      builder: (FormFieldState<dynamic> field) {
        return InputDecorator(
          decoration: InputDecoration(

            contentPadding: EdgeInsets.only(top: 10.0, bottom: 0.0),
            border: InputBorder.none,
            errorText: widget.incomeError == 1 ? 'Please Choose' : null,
          ),
          child: Container(
            decoration: BoxDecoration(
                color: AppColor.wildSand,
                borderRadius: BorderRadius.all(Radius.circular(10)),
               ),
            padding: EdgeInsets.only(top:15, ),
            child: DropdownButtonFormField(
              decoration: InputDecoration(
                labelText: 'Income for activation',

                contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                fillColor: AppColor.wildSand,
                filled: true,
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide.none),
              ),
              // hint: Text(_hint),
              isExpanded: true,
              items: itemsList.map((option) {
                return DropdownMenuItem(
                  child: Text("$option"),
                  value: option,
                );
              }).toList(),
              value:widget.incomeValue == ""? "-Choose Option-": itemsList.contains(
                  widget.incomeValue)
                  ?widget.incomeValue: null,
              onChanged: widget.function,
            ),
          ),
        );
      },
    );

  }
}
