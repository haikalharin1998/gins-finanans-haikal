import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:gin_finans_haikal/router_constants.dart';

class Routes {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      // case RouteName.register1:
        // return MaterialPageRoute(builder: (_) => Register1Page());

      default:
        return MaterialPageRoute(
          builder: (_) =>
              Scaffold(
                body: Center(
                    child: Text('No route defined for ${settings.name}')),
              ),
        );
    }
  }
}