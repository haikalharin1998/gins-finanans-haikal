import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gin_finans_haikal/theme/theme_color.dart';


class WaktuDropdown extends StatefulWidget {
  Function() function;
  int waktuError = 0;
  String waktuValue = "";
  WaktuDropdown(this.function, this.waktuValue, this.waktuError);

  @override
  _WaktuDropdownState createState() => _WaktuDropdownState();
}

class _WaktuDropdownState extends State<WaktuDropdown> {
  final List<String> itemsList = <String>["-Choose Option-",
  ];



  @override
  Widget build(BuildContext context) {
    if (!itemsList.contains(widget.waktuValue)) {
      itemsList.add(widget.waktuValue);
    }
    // TODO: implement build
    return InkWell(
      onTap: widget.function,
      child: FormField(
        // key: _fieldKey,
        enabled: true,
        builder: (FormFieldState<dynamic> field) {
          return InputDecorator(
            decoration: InputDecoration(

              contentPadding: EdgeInsets.only(top: 10.0, bottom: 0.0),
              border: InputBorder.none,
              errorText: widget.waktuError == 1 ? 'Please Choose' : null,
            ),
            child: Container(
              decoration: BoxDecoration(
                color: AppColor.wildSand,
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              padding: EdgeInsets.only(top:15, ),
              child: DropdownButtonFormField(
                decoration: InputDecoration(
                  labelText: 'Date',

                  contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  fillColor: AppColor.wildSand,
                  filled: true,
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      borderSide: BorderSide.none),
                ),
                // hint: Text(_hint),
                isExpanded: true,
                items: itemsList.map((option) {
                  return DropdownMenuItem(
                    child: Text("$option"),
                    value: option,
                  );
                }).toList(),
                value: widget.waktuValue == ""
                    ?"-Choose Option-": itemsList.contains(
                    widget.waktuValue)
                    ?widget.waktuValue: null,
                // onChanged: widget.function,
              ),
            ),
          );
        },
      ),
    );

  }
}
