import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gin_finans_haikal/theme/theme_color.dart';

class Step4 extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 50),
        Row(
          children: [
            Expanded(
                child: Container(
                    margin: EdgeInsets.only(left: 0),
                    child: Text(
                      "Thank You",
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                    ))),
          ],
        ),
        Row(
          children: [
            Expanded(
                child: Container(
                    margin: EdgeInsets.only(top: 10, bottom: 20),
                    child: Text(
                      "thank you for the opportunity that has been given,"
                          " hopefully after this there is good news and we can work together",
                      style: TextStyle(fontSize: 15, color: AppColor.wildSand),
                    ))),
          ],
        ),
        Row(
          children: [
            Container(
                margin: EdgeInsets.only( bottom: 20),
                child: Text(
                  "✉ ",
                  style: TextStyle(fontSize: 30, color: AppColor.wildSand),
                )),
            Container(
                margin: EdgeInsets.only(top: 10, bottom: 20),
                child: Text(
                  ": haikalharin1998@gmail.com ",
                  style: TextStyle(fontSize: 15, color: AppColor.wildSand),
                )),
          ],
        ),

      ],
    );
  }
}
