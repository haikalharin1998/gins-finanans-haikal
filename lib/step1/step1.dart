import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gin_finans_haikal/theme/theme_color.dart';
import 'widget/password_input.dart';
import 'widget/validasi_bar.dart';

class Step1 extends StatefulWidget {
  int page = 0;
  String passwordValue;
  bool passwordError;
  Function(String) function;

  Step1(this.page, this.passwordValue, this.passwordError, this.function);

  @override
  _Step1State createState() => _Step1State();
}

class _Step1State extends State<Step1> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 50),
        Row(
          children: [
            Expanded(
                child: Container(
                    margin: EdgeInsets.only(left: 0, bottom: 10),
                    child: Text(
                      "Create Password",
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: AppColor.wildSand),
                    ))),
          ],
        ),
        Row(
          children: [
            Expanded(
                child: Container(
                    margin: EdgeInsets.only(left: 0, bottom: 50),
                    child: Text(
                      "Password will be used to login to account",
                      style: TextStyle(fontSize: 15, color: AppColor.wildSand),
                    ))),
          ],
        ),
        Widget_PasswordInput(widget.function, widget.passwordError),
        SizedBox(height: 20),
        Container(
          child: Row(
            children: [
              Container(
                  margin: EdgeInsets.only(right: 5),
                  child: Center(
                    child: Text(
                      "Complexity :",
                      style: TextStyle(color: AppColor.wildSand, fontSize: 12),
                    ),
                  )),
              Container(
                  child: Center(
                child: Text(
                  widget.passwordValue == "" || widget.passwordError == true
                      ? "-"
                      : widget.passwordValue.length < 12
                          ? "Very Weak"
                          : widget.passwordValue.length < 15
                              ? "Weak"
                              : "strong",
                  style: TextStyle(
                      color: widget.passwordValue == "" ||
                              widget.passwordError == true
                          ? AppColor.wildSand
                          : widget.passwordValue.length < 15
                              ? AppColor.warna_alert
                              : AppColor.green,
                      fontSize: 12,
                      fontWeight: FontWeight.bold),
                ),
              )),
            ],
          ),
        ),
        SizedBox(height: 150),
        Widget_ValidasiBar(widget.passwordValue, widget.page)
      ],
    );
  }
}
