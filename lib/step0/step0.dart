import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gin_finans_haikal/theme/theme_color.dart';

class Step0 extends StatelessWidget {
  TextEditingController _email;
  Function(String) function;
  bool emailError;

  Step0(this._email, this.emailError, this.function);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 100),
        Row(
          children: [
            Expanded(
                child: Container(
                    margin: EdgeInsets.only(left: 15),
                    child: Text(
                      "Welcome to",
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                    ))),
          ],
        ),
        Row(
          children: [
            Container(
                margin: EdgeInsets.only(left: 15),
                child: Text(
                  "GIN",
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                )),
            Container(
                margin: EdgeInsets.only(left: 8),
                child: Text(
                  "Finans",
                  style: TextStyle(
                      fontSize: 30,
                      color: AppColor.deepCerulean,
                      fontWeight: FontWeight.bold),
                )),
          ],
        ),
        Row(
          children: [
            Expanded(
                child: Container(
                    margin: EdgeInsets.only(left: 15),
                    child: Text(
                      "Welcome to The Bank of The Future"
                      "\nMenage and track your accounts on"
                      "\nthe go",
                      style: TextStyle(fontSize: 15),
                    ))),
          ],
        ),
        Container(
          padding: EdgeInsets.all(20),
          margin: EdgeInsets.fromLTRB(10, 20, 10, 0),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
            // boxShadow: <BoxShadow>[
            //   BoxShadow(blurRadius: 3),
            //   BoxShadow(color: Colors.blueGrey[50])
            // ]
          ),
          child: TextFormField(
            key: key,
            inputFormatters: [
              LengthLimitingTextInputFormatter(60),
            ],
            controller: _email,
            onChanged: function,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.only(
                  top: 21.0, left: 10.0, right: 10.0, bottom: 21.0),
              fillColor: AppColor.wildSand,
              filled: true,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10.0),
                borderSide: BorderSide(color: AppColor.white),
              ),
//            prefixIcon: Icon(
//              Icons.attach_money,
//              color: AppColor.warna_teks_sub,
//            ),
              hintStyle: TextStyle(color: AppColor.warna_teks_sub),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: AppColor.wildSand),
                borderRadius: BorderRadius.circular(10.0),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: AppColor.wildSand),
                borderRadius: BorderRadius.circular(10.0),
              ),
              hintText: 'Email',
              errorText: emailError == true ? 'invalid Email' : null,
            ),
          ),
        ),
      ],
    );
  }
}
