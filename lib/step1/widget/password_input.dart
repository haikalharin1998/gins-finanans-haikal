import 'package:flutter/material.dart';
import 'package:gin_finans_haikal/theme/theme_color.dart';


class Widget_PasswordInput extends StatefulWidget {
  Function(String) function;
  bool passwordError;
  Widget_PasswordInput(this.function,this.passwordError);
  @override
  _Widget_PasswordInputState createState() => _Widget_PasswordInputState();
}

class _Widget_PasswordInputState extends State<Widget_PasswordInput> {
  bool _isHidden = true;

  bool _isHiddenTap= true;

  final _tecPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // _tecPassword.text = '12345678';
    // Injector.resolve<Bloc>().add(SelectHidden(true));
    // Injector.resolve<Bloc>()
    //     .add(PasswordChangedEvent(_tecPassword.text));
        _isHidden = _isHidden == null ? true : _isHidden;
        return TextFormField(
          validator: (value) => widget.passwordError ? 'invalid password' : null ,
          obscureText: _isHidden,
          // controller: _tecPassword,
//          key: const Key('Form_passwordInput_textField'),
          onChanged: widget.function,
          decoration: InputDecoration(
            hintText: 'Password',
            suffixIcon: GestureDetector(
              onTap: () {
                _isHiddenTap = _isHidden == true ? false : true;
//                if(state.isHidden == true){
//                  _isHiddenTap = false;
//                } else { _isHiddenTap = true;}
                setState(() {
                  _isHidden =_isHiddenTap;
                });
              },
              child: Icon(
                _isHidden == true ? Icons.visibility_off : Icons.visibility,
                color: _isHidden == true ? AppColor.gray : AppColor.warna_utama,
              ),
            ),
            isDense: true,
            contentPadding: EdgeInsets.only(
                top: 21.0, left: 10.0, right: 10.0, bottom: 21.0),
            fillColor: AppColor.white,
            filled: true,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
              borderSide: BorderSide(color: AppColor.wildSand),),
//            prefixIcon: Icon(
//              Icons.attach_money,
//              color: AppColor.warna_teks_sub,
//            ),
            hintStyle: TextStyle(color: AppColor.warna_teks_sub),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: AppColor.wildSand),
              borderRadius: BorderRadius.circular(10.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: AppColor.wildSand),
              borderRadius: BorderRadius.circular(10.0),
            ),
            errorText: widget.passwordError ? 'does not meet the criteria' : null,
          ),
        );
  }
}
