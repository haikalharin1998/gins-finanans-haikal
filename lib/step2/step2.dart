import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gin_finans_haikal/step2/widget/expense_dropdown.dart';
import 'package:gin_finans_haikal/step2/widget/goal_dropdown.dart';
import 'package:gin_finans_haikal/step2/widget/income_dropdown.dart';
import 'package:gin_finans_haikal/theme/theme_color.dart';

class Step2 extends StatefulWidget {
  // int page = 0;
  // String passwordValue;
  // bool passwordError;
  String goalValue = "";
  int goalError = 0;
  String incomeValue = "";
  int incomeError = 0;
  String expenseValue = "";
  int expenseError = 0;
  Function(Object?) goalFunction;
  Function(Object?) incomeFunction;
  Function(Object?) expenseFunction;

  Step2({
    // this.page, this.passwordValue,
    this.goalValue = "",
    this.goalError = 0,
    required this.goalFunction,
    this.incomeValue = "",
    this.incomeError = 0,
    required this.incomeFunction,
    this.expenseValue = "",
    this.expenseError = 0,
    required this.expenseFunction,
  });

  @override
  _Step2State createState() => _Step2State();
}

class _Step2State extends State<Step2> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 40),
        Row(
          children: [
            Expanded(
                child: Container(
                    margin: EdgeInsets.only(left: 0, bottom: 10),
                    child: Text(
                      "Schedule Video Call",
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: AppColor.wildSand),
                    ))),
          ],
        ),
        Row(
          children: [
            Expanded(
                child: Container(
                    margin: EdgeInsets.only(left: 0, bottom: 20),
                    child: Text(
                      "Choose the date and time that you preffered. we "
                          "will send a link via email to make a video call on"
                          "the scheduled date and time",
                      style: TextStyle(fontSize: 15, color: AppColor.wildSand),
                    ))),
          ],
        ),
        GoalDropdown(widget.goalFunction, widget.goalValue, widget.goalError),
        IncomeDropdown(
            widget.incomeFunction, widget.incomeValue, widget.incomeError),
        ExpenseDropdown(
            widget.expenseFunction, widget.expenseValue, widget.expenseError)
      ],
    );
  }
}
