import 'package:flutter/cupertino.dart';
import 'package:gin_finans_haikal/theme/theme_color.dart';

class Ceklis extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: 40,
        height: 40,
        decoration: new BoxDecoration(
          border: Border.all(color: AppColor.green, width: 2),
          color: AppColor.green,
          shape: BoxShape.circle,
        ),
        child: Center(
          child: Text(
            "✔",
            style: TextStyle(color: AppColor.wildSand, fontSize: 20),
          ),
        ));
  }
}
